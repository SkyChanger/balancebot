from datetime import datetime
import ctypes
import ccxt
import functools
import pickle
import pandas as pd
from exchanges.CommonExchangeFuncs import handle_balance_Data

def getStart():
    return datetime(year=2020, month=9, day=1, hour=2).timestamp()

def getNow():
    return datetime.now().timestamp()

def getStartInStamp():
    return datetime(year=2020, month=9, day=1, hour=2)

def init_ccxt(key, secret) -> ccxt.Exchange:
    try:
        api = getattr(ccxt, 'bitmex')({
            'apiKey': key,
            'secret': secret,
            'enableRateLimit': True,
            'rateLimit': 2000,
        })

    except (KeyError, AttributeError) as error:
        print(error)

    return api


def getAllMyTrades(ccxtApi):
    all_trades = dict({})
    starting = getStart() * 1000

    while(starting < ccxtApi.milliseconds()):
        symbol_trade_history = ccxtApi.fetch_ledger(since=starting, limit= 200)
        if len(symbol_trade_history):
            starting = symbol_trade_history[len(symbol_trade_history) - 1]['timestamp'] + 1
            for trade in symbol_trade_history:
                if(trade['status'] == "ok" and getStart() < trade['timestamp']):
                    all_trades[str(trade['timestamp']) + '_' + str(trade['info']['transactID'])] = trade['info']
        else:
            break
    return all_trades


def getAccountInformation(loginData: dict = None):

    ccxtApi = init_ccxt(loginData['key'], loginData['secret'])
    all_trades = getAllMyTrades(ccxtApi)

    pnls = sorted(list(map(lambda item: dict({'realizedPnl': float(item['amount']), 'timestamp': item['transactTime']}), all_trades.values())), key=lambda k: k['timestamp'])
    current_balance = int(ccxtApi.fetchBalance()['info'][0]['walletBalance'])

    return handle_balance_Data(pnls, current_balance, getStartInStamp, getNow)
