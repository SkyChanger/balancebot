import functools
import pandas as pd
from datetime import datetime

def getDelta(first, second):
    diff = second - first
    return diff / first * 100

def handle_balance_Data(profits_and_losses, current_balance, getStart, getNow, pandas_time_unit=None):
    total_pnl = functools.reduce(lambda prev, value: prev + float(value['realizedPnl']), profits_and_losses, 0.0)
    starting_balance = current_balance - total_pnl

    pnls = sorted(list(map(lambda item: dict({'realizedPnl': float(item['realizedPnl']), 'timestamp': item['timestamp']}), profits_and_losses)), key=lambda k: k['timestamp'])

    pnl_series = []
    pnl_series.append(dict({
        'timestamp': getStart(),
        'balance': starting_balance,
        'realizedPnl': 0,
        'total_pnl': 0,
        'delta_to_start': 0,
    }))

    for pnl in pnls:
        entry = dict({
            'timestamp': pnl['timestamp'],
            'balance': (pnl_series[-1]['balance'] + pnl['realizedPnl']),
            'realizedPnl': pnl['realizedPnl'],
            'total_pnl': (pnl_series[-1]['total_pnl'] + pnl['realizedPnl']),
            'delta_to_start': getDelta(starting_balance, starting_balance + (pnl_series[-1]['total_pnl'] + pnl['realizedPnl'])) if len(pnl_series) >=2 else 0,
        })
        pnl_series.append(entry)

    # If PNL is only 1 entry, to get two points for a line, we need beginning and end
    if len(pnl_series) == 1:
        pnl_series.append({
            'timestamp': getNow(),
            'balance': pnl_series[-1]['balance'],
            'realizedPnl': 0,
            'total_pnl': pnl_series[-1]['total_pnl'],
            'delta_to_start': pnl_series[-1]['delta_to_start']
        })
    
    pnl_df = pd.DataFrame(pnl_series)

    if(pandas_time_unit == None):
        pnl_df['timestamp'] = pd.to_datetime(pnl_df.timestamp)
    else:
        print("Stamp Given")
        print(pandas_time_unit)
        pnl_df['timestamp'] = pd.to_datetime(pnl_df.timestamp, unit=pandas_time_unit)

    balance_data = {
        'starting_balance': starting_balance,
        'current_balance': current_balance,
        'pnl_df': pnl_df
    }

    return balance_data
