from datetime import datetime
import requests
import ctypes
import functools
import pickle
import pandas as pd
import hmac
import json
import time
from exchanges.CommonExchangeFuncs import handle_balance_Data

BASE_URL = 'https://api.bybit.com'
BALANCE = '/v2/private/wallet/balance'
PNL = '/v2/private/trade/closed-pnl/list'

def get_signature(secret: str, req_params: dict):
    """
    :param secret    : str, your api-secret
    :param req_params: dict, your request params
    :return: signature
    """
    _val = '&'.join([str(k)+"="+str(v) for k, v in sorted(req_params.items()) if (k != 'sign') and (v is not None)])
    return str(hmac.new(bytes(secret, "utf-8"), bytes(_val, "utf-8"), digestmod="sha256").hexdigest())

def getStart():
    return int(datetime(year=2020, month=9, day=1, hour=2).timestamp())

def getNow():
    return int(datetime.now().timestamp())

def getPnlData(key, secret, symbol):
    params = dict()
    params['api_key'] = key
    params['symbol'] = symbol
    params['timestamp'] = str(int(round(time.time())-1))+"000"
    params['sign'] = get_signature(secret, params)
    
    from_time = getStart()
    endTime = str(int(round(time.time())-1))+"000"

    pnls = []
    pnls.append(dict({
        'timestamp': getStart(),
        'balance': 0,
        'realizedPnl': 0,
        'total_pnl': 0,
        'delta_to_start': 0,
    }))

    known_order_ids = []
    while(int(from_time) < int(endTime)):
        data = json.loads(requests.get(BASE_URL + PNL, params).text)
        if(data['result']['data'] is not None):
            data_array = list(filter(lambda x: x['id'] not in known_order_ids, data['result']['data']))
            if len(data_array):
                for entry in data['result']['data']:
                    known_order_ids.append(entry['id'])
                    pnls.append(dict({
                        'timestamp': entry['created_at'],
                        'realizedPnl': entry['closed_pnl'],
                    }))
                    from_time = entry['created_at'] + 1
            else:
                break
        else:
            break
        time.sleep(1)

    return pnls

def getTotalBalance(key, secret):
    params = dict()
    params['api_key'] = key
    params['timestamp'] = str(int(round(time.time())-1))+"000"
    params['sign'] = get_signature(secret, params)
    return json.loads(requests.get(BASE_URL + BALANCE, params = params).text)['result']['BTC']['equity']

def getAccountInformation(loginData: dict = None):

    profits_and_losses = getPnlData(loginData['key'], loginData['secret'], 'BTCUSD')
    current_balance = getTotalBalance(loginData['key'], loginData['secret'])

    return handle_balance_Data(profits_and_losses, current_balance, getStart, getNow, pandas_time_unit='s')
