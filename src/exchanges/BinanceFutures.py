from datetime import datetime
import ctypes
import ccxt
import functools
import pickle
import pandas as pd
from exchanges.CommonExchangeFuncs import handle_balance_Data

def getStart():
    return datetime(year=2020, month=9, day=1, hour=2).timestamp() * 1000

def getNow():
    return datetime.now().timestamp() * 1000

def init_ccxt(key, secret) -> ccxt.Exchange:
    try:
        api = getattr(ccxt, 'binance')({
            'apiKey': key,
            'secret': secret,
            'enableRateLimit': True,
            'rateLimit': 1000,
            'options': {
                'defaultType': 'future'
            },
        })
    except (KeyError, AttributeError) as error:
        print(error)
    return api


def getDelta(first, second):
    diff = second - first
    return diff / first * 100


def getAllMyTrades(ccxtApi):
    symbols = [item['symbol'] for item in ccxtApi.fetchMarkets()]
    all_trades = dict({})
    starting = getStart()
    for sym in symbols:
        while(starting < ccxtApi.milliseconds()):
            symbol_trade_history = ccxtApi.fetchMyTrades(symbol=sym, since=starting, limit= 1000)
            if len(symbol_trade_history):
                starting = symbol_trade_history[len(symbol_trade_history) - 1]['timestamp'] + 1
                for trade in symbol_trade_history:
                        all_trades[str(trade['timestamp']) + '_' + str(trade['info']['orderId'])] = trade['info']     
            else:
                break
    return all_trades


def getAccountInformation(loginData: dict = None):
    ccxtApi = init_ccxt(loginData['key'], loginData['secret'])
    all_trades = getAllMyTrades(ccxtApi)
    
    pnls = sorted(list(map(lambda item: dict({'realizedPnl': float(
        item['realizedPnl']), 'timestamp': item['time']}), all_trades.values())), key=lambda k: k['timestamp'])
    current_balance = ccxtApi.fetchBalance()['total']['USDT']

    return handle_balance_Data(pnls, current_balance, getStart, getNow, pandas_time_unit='ms')
